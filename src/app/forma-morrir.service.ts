import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})


export class FormaMorrirService {

  formaMorir: any[] = [
    {
      forma: 1,
      nombre: 'Globo-loco',
      descripcion: '"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. '
  
    },
    {
      forma: 2,
      nombre: 'El patinador estampado',
      descripcion: '"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. '
  
    },
    {
      forma: 3,
      nombre: 'Pechos asfixiantes',
      descripcion: '"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. '
  
    },
    {
      forma: 4,
      nombre: 'La llamada de la muerte',
      descripcion: '"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. '
  
    },
  ]
  constructor() { }

  obtenerMuertes(){
    return this.formaMorir;
  }
}
